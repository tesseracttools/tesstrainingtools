/*
 * Copyright 2012 Nick White <nick.white@durham.ac.uk>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * This reads an image file and tesseract box file, and outputs an
 * image file overlaid with information from the box.
 *
 * Build it with something like this:
 * cc `pkg-config --cflags --libs pangocairo` boxer.c -o boxer
 */

#define usage "boxer - overlays an image with box information\n" \
              "usage: boxer in.png in.box out.png\n"

#include <stdio.h>
#include <stdlib.h>
#include <cairo.h>
#include <pango/pangocairo.h>

#define MAXGLYPHBYTES 24 /* the maximum amount tesseract 3 will handle */
#define FONTSIZE 12

typedef struct {
	unsigned char glyph[MAXGLYPHBYTES];
	unsigned int x;
	unsigned int y;
	long width;
	long height;
	unsigned int pg;
} Box;

int main(int argc, char *argv[]) {
	char buf[BUFSIZ];
	unsigned int boxlen, i;
	unsigned int x2, y2;
	int imgy;
	Box *boxen = NULL;
	Box *b = NULL;
	FILE *box;
	cairo_surface_t *surface;
	cairo_t *cr;
	cairo_status_t err;
	PangoLayout *layout;
	PangoFontDescription *font_description;

	if(argc != 4) {
		fputs(usage, stdout);
		return 1;
	}

	if((box = fopen(argv[2], "r")) == NULL) {
		fprintf(stderr, "Can't open box file: %s\n", argv[2]);
		return 1;
	}

	surface = cairo_image_surface_create_from_png(argv[1]);
	if((err = cairo_surface_status(surface)) != CAIRO_STATUS_SUCCESS) {
		fprintf(stderr, "Error opening image file: %s - %s\n",
		        argv[1], cairo_status_to_string(err));
		fclose(box);
		return 1;
	}
	cr = cairo_create(surface);
	layout = pango_cairo_create_layout(cr);
	font_description = pango_font_description_new();
	pango_font_description_set_absolute_size(font_description, FONTSIZE * PANGO_SCALE);
	pango_layout_set_font_description(layout, font_description);

	/* parse box file into Box struct */
	for(boxlen=0; !feof(box); boxlen++) {
		boxen = (Box *)realloc(boxen, sizeof(*boxen) * (boxlen + 1));
		fgets(buf, sizeof(buf) - 1, box);
		b = &(boxen[boxlen]);
		sscanf(buf, "%s %u %u %u %u %u",
		       b->glyph, &(b->x), &(b->y), &x2, &y2, &(b->pg));
		b->width = x2 - b->x;
		b->height = b->y - y2;
	}

	imgy = cairo_image_surface_get_height(surface);

	/* draw rectangles around boxes */
	cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
	for(i=0, b=boxen; i<boxlen; i++, b++) {
		if(b->pg != 0) {
			continue;
		}
		cairo_rectangle(cr, b->x, imgy - b->y, b->width, b->height);
		cairo_stroke(cr);
	}

	/* draw glyph number */
	cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
	for(i=0, b=boxen; i<boxlen; i++, b++) {
		if(b->pg != 0) {
			continue;
		}
		snprintf(buf, 8, "%u", i + 1);
		cairo_move_to(cr, b->x, imgy - b->y + b->height + 1);
		pango_layout_set_text(layout, buf, -1);
		pango_cairo_show_layout(cr, layout);
	}

	/* draw box glyph */
	cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);
	for(i=0, b=boxen; i<boxlen; i++, b++) {
		if(b->pg != 0) {
			continue;
		}
		cairo_move_to(cr, b->x, imgy - b->y - FONTSIZE - 4);
		pango_layout_set_text(layout, (const char *)b->glyph, -1);
		pango_cairo_show_layout(cr, layout);
	}

	pango_font_description_free(font_description);                                             
	g_object_unref(layout);                                                                    
	cairo_destroy(cr);                                                                         
	if((err = cairo_surface_write_to_png(surface, argv[3])) != CAIRO_STATUS_SUCCESS) {         
		fprintf(stderr, "Error writing image file: %s\n", cairo_status_to_string(err));    
	}                                                                                          
	cairo_surface_destroy(surface);

	fclose(box);
	return 0;
}
