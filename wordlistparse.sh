#!/bin/sh
# Copyright 2012 Nick White <nick.white@durham.ac.uk>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

usage="Usage: $0 all-words freq-words

Takes a wordlist in stdin, one word per line, and outputs two
files, all-words, containing all words, and freq-words,
containing the most frequent words."

test $# -ne 2 && echo "$usage" && exit 1

a=`sort | uniq -c | sort -n`

echo "$a" | awk '{print $2}' | sort > "$1"
echo "$a" | awk '{if ($1 > 1000) {print $2}}' | sort > "$2"
