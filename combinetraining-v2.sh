#!/bin/sh
# Copyright 2012 Nick White <nick.white@durham.ac.uk>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

usage="Usage: $0 [-l langcode] trainingdir1 [trainingdir2...]

Creates Tesseract 2 training files.

Looks for .png, .box, freq-words, all-words and unicharambigs
files in each training directory.

Note that if files of the same name are found in different training
directories they won't be copied, so ensure there are no name collisions."

lang=grc

test $# -lt 1 && echo "$usage" && exit 1

t=`mktemp -d`
orig=`pwd`

echo "Combining training source files"
while `test -n "$1"`; do
	if `test "$1" = "-l"`; then
		shift
		lang="$1"
	else
		# copy all files with these extensions
		for i in png box unicharambigs; do
			if ls "$1"/*$i >/dev/null 2>&1; then
				cp "$1"/*$i "$t/"
			fi
		done
		for i in freq-words all-words; do
			test -f "$1/$i" && cat "$1/$i" >> "$t/$i"
		done
	fi
	shift
done

cd "$t"

echo "Making .tr trainings"
# tesseract 2's mftraining can only process up to about 20 .trs,
# or it will segfault (really), so restrict them here.
find . -name '*png' | head -n 15 | while read i; do
	b=`basename "$i" .png`
	convert "$i" -depth 4 -colorspace Gray -background white -flatten +matte -density 600x600 "$b.tif"
	tesseract "$b.tif" "$b" nobatch box.train.stderr
done

echo "Clustering"
mftraining $lang*tr
cntraining $lang*tr

echo "Getting list of all possible characters"
unicharset_extractor *box

echo "Creating word lists"
wordlist2dawg freq-words freq-dawg
wordlist2dawg all-words word-dawg
touch user-words

echo "Combining training files"
mkdir tessdata
for i in normproto inttemp pffmtable unicharset freq-dawg word-dawg user-words; do
	cp "$i" "tessdata/$lang.$i"
done
test -f "$lang.unicharambigs" && cp "$lang.unicharambigs" "tessdata/$lang.DangAmbigs"
tar c tessdata | gzip > "tesseract-2.00.$lang.tar.gz"

cd "$orig"
cp "$t/tesseract-2.00.$lang.tar.gz" .
echo "Done. tesseract-2.00.$lang.tar.gz should now be usable"
