/*
 * Copyright 2013 Nick White <nick.white@durham.ac.uk>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * This creates an image file with specified text in a specified font,
 * with a corresponding box file, to be used to train Tesseract.
 *
 * Build it with something like this:
 * cc `pkg-config --cflags --libs pangocairo` libutf/rune.c libutf/utf.c util/runebody.c lazytrain.c -o lazytrain
 *
 * "I'm going off the rails on this crazy train..."
 */

#define usage "lazytrain - creates an image with characters and a corresponding box file\n" \
              "usage: lazytrain text.txt fontname out.png out.box\n" \
              "text.txt - a UTF-8 text file containing all characters to include\n" \
              "fontname - pango font string (see below)\n\n" \
              "Pango font strings are in the form 'family [style] [size]', e.g.\n" \
              "'GFS Didot 24px' or 'Arial Bold' or 'Serif'.\n"\
              "The font must be installed before you can use it with lazytrain.\n" \
              "To install a new font, do the following:\n" \
              "  Linux: copy the font file you want to $HOME/.fonts\n" \
              "  Windows: go to C:\\windows\\fonts and File->Install New Font\n"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cairo.h>
#include <pango/pangocairo.h>
#include "libutf/utf.h"
#include "util/runetype.h"

#define DEFAULTFONTSIZE 48
#define XPADDING 10
#define YPADDING 20
#define MAXCOMBININGCHARS 20
#define BOXPADDING 1

typedef struct {
	unsigned int x;
	unsigned int y;
	unsigned int xbox;
	unsigned int ybox;
	unsigned int width;
	unsigned int height;
	char c[(UTFmax * MAXCOMBININGCHARS)];
	unsigned int len;
} Box;

void cairo_surface_blur(cairo_surface_t* surface, double radius);

int main(int argc, char *argv[]) {
	char c[UTFmax];
	Rune *runes = NULL;
	char buf[BUFSIZ] = "";
	int x, y, lineheight;
	int pgwidth, pgheight;
	int fontsize;
	unsigned int runenum, runepos, boxnum, len;
	size_t i, n;
	FILE *f;
	Box **boxes = NULL;
	Box *box;
	cairo_surface_t *surface;
	cairo_t *cr;
	cairo_status_t err;
	PangoLayout *layout;
	PangoFontDescription *font_description;
	PangoRectangle rect;

	if(argc != 5) {
		fputs(usage, stdout);
		return 1;
	}

	if((f = fopen(argv[1], "r")) == NULL) {
		fprintf(stderr, "Can't open char file: %s\n", argv[1]);
		return 1;
	}

	runenum = 0;
	runepos = 0;
	for(i = 0; (n = fread(&buf[i], 1, sizeof buf - i, f)); i = n-i) {
		buf[n] = 0;
		runenum += utflen(buf);
		runes = (Rune *)realloc(runes, sizeof(*runes) * (runenum + 1));
		for(n += i, i = 0; (len = charntorune(&runes[runepos], &buf[i], n-i)); i += len, runepos++);
	}

	fclose(f);

	if((f = fopen(argv[4], "w")) == NULL) {
		fprintf(stderr, "Can't open box file: %s\n", argv[4]);
		return 1;
	}

	font_description = pango_font_description_from_string(argv[2]);
	if(!(pango_font_description_get_set_fields(font_description) & PANGO_FONT_MASK_SIZE)) {
		pango_font_description_set_absolute_size(font_description, DEFAULTFONTSIZE * PANGO_SCALE);
	}
	fontsize = pango_font_description_get_size(font_description);
	if(pango_font_description_get_size_is_absolute(font_description)) {
		fontsize /= PANGO_SCALE;
	}

	/* set page width proportional to font size, to vaguely large enough to hold ~60 chars */
	pgwidth = (fontsize + YPADDING) * 30;

	/* create temporary surface for font calculations */
	surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, pgwidth, fontsize * 2);
	cr = cairo_create(surface);
	layout = pango_cairo_create_layout(cr);
	pango_layout_set_font_description(layout, font_description);

	boxes = (Box **)malloc(sizeof(*boxes) * (runenum + 1));
	for(i=0; i<runenum; i++) {
		boxes[i] = (Box *)malloc(sizeof(**boxes));
		boxes[i]->len = 0;
		boxes[i]->c[0] = 0;
	}
	lineheight = YPADDING;
	rect.width = rect.height = 0;
	for(i=0, boxnum=0, x=XPADDING, y=YPADDING + fontsize, box = boxes[0];
	    i<runenum;
	    i++, boxnum++, box = boxes[boxnum]) {
		if(iscombiningrune(runes[i])) {
			box = boxes[--boxnum];
		} else {
			/* add space for previous character */
			x += rect.width + XPADDING;
		}
		len = runetochar(c, &runes[i]);
		memmove(box->c+box->len, c, len);
		box->len += len;
		/* skip unprintable characters */
		if(box->c[0] == '\t' || box->c[0] == '\r' || box->c[0] == 0) {
			box->c[0] = 0;
			continue;
		}
		pango_layout_set_text(layout, box->c, box->len);
		pango_layout_get_pixel_extents(layout, &rect, NULL);

		/* handle linebreaks */
		if(box->c[0] == '\n' || x + rect.width + XPADDING > pgwidth) {
			y += YPADDING + lineheight;
			lineheight = YPADDING;
			x = XPADDING;
		}
		if(box->c[0] == '\n') {
			box->c[0] = 0;
			continue;
		}

		if(rect.height > lineheight) {
			lineheight = rect.height;
		}

		box->x = x;
		box->y = y;
		box->xbox = x + rect.x;
		box->ybox = y + rect.y;
		box->width = rect.width;
		box->height = rect.height;
	}
	pgheight = y + YPADDING;

	free(runes);
	g_object_unref(layout);
	cairo_destroy(cr);
	cairo_surface_destroy(surface);

	/* create final surface for drawing */
	surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, pgwidth, pgheight);
	cr = cairo_create(surface);
	layout = pango_cairo_create_layout(cr);
	pango_layout_set_font_description(layout, font_description);

	/* set background to white */
	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
	cairo_paint(cr);
	cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);

	/* use runenum, not boxnum, to ensure even empty boxes are all freed */
	for(i = 0, box = boxes[0]; i < runenum; box = boxes[++i]) {
		if(box->c[0]) {
			cairo_move_to(cr, box->x, box->y);
			pango_layout_set_text(layout, box->c, box->len);
			pango_cairo_show_layout(cr, layout);

			if(box->c[0] != ' ') {
				fwrite(box->c, box->len, 1, f);
				fprintf(f, " %d %d %d %d %d\n",
				        box->xbox - BOXPADDING,
				        pgheight - (box->ybox + box->height) - BOXPADDING,
				        box->xbox + box->width + BOXPADDING,
				        pgheight - box->ybox + BOXPADDING, 0);
			}
		}
		free(box);
	}
	fclose(f);
	free(boxes);

	/* A little blurring increases Tesseract's recognition */
	if(fontsize >= 24) {
		cairo_surface_blur(surface, 1.0);
	}

	pango_font_description_free(font_description);
	g_object_unref(layout);
	cairo_destroy(cr);
	if((err = cairo_surface_write_to_png(surface, argv[3])) != CAIRO_STATUS_SUCCESS) {
		fprintf(stderr, "Error writing image file: %s\n", cairo_status_to_string(err));
	}
	cairo_surface_destroy(surface);

	return 0;
}

void cairo_surface_blur(cairo_surface_t* surface, double radius)
{
	// from http://stevehanov.ca/blog/index.php?id=53
	// Steve Hanov, 2009
	// Released into the public domain.
	
	// get width, height
	int width = cairo_image_surface_get_width(surface);
	int height = cairo_image_surface_get_height(surface);
	unsigned char* dst = (unsigned char*)malloc(width*height*4);
	unsigned* precalc = (unsigned*)malloc(width*height*sizeof(unsigned));
	unsigned char* src = cairo_image_surface_get_data(surface);
	double mul=1.f/((radius*2)*(radius*2));
	int channel;
	
	// The number of times to perform the averaging. According to wikipedia,
	// three iterations is good enough to pass for a gaussian.
	const int MAX_ITERATIONS = 3; 
	int iteration;

	memcpy(dst, src, width*height*4);

	for(iteration = 0; iteration < MAX_ITERATIONS; iteration++) {
		for(channel = 0; channel < 4; channel++) {
			int x,y;

			// precomputation step.
			unsigned char* pix = src;
			unsigned* pre = precalc;

			pix += channel;
			for(y=0;y<height;y++) {
				for(x=0;x<width;x++) {
					int tot=pix[0];
					if(x>0) tot+=pre[-1];
					if(y>0) tot+=pre[-width];
					if(x>0 && y>0) tot-=pre[-width-1];
					*pre++=tot;
					pix += 4;
				}
			}

			// blur step.
			pix = dst + (int)radius * width * 4 + (int)radius * 4 + channel;
			for(y=radius;y<height-radius;y++) {
				for(x=radius;x<width-radius;x++) {
					int l = x < radius ? 0 : x - radius;
					int t = y < radius ? 0 : y - radius;
					int r = x + radius >= width ? width - 1 : x + radius;
					int b = y + radius >= height ? height - 1 : y + radius;
					int tot = precalc[r+b*width] + precalc[l+t*width] - 
						precalc[l+b*width] - precalc[r+t*width];
					*pix=(unsigned char)(tot*mul);
					pix += 4;
				}
				pix += (int)radius * 2 * 4;
			}
		}
		memcpy(src, dst, width*height*4);
	}

	free(dst);
	free(precalc);
}
