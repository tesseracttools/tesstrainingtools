#!/bin/sh
# Copyright 2012 Nick White <nick.white@durham.ac.uk>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# Requires lazytrain.

usage="Usage: $0 [-l lang] fontdir textfile savedir

Creates an image and box file for a text for each font in the
fontdir, and an appropriate font_properties file.

-l lang: language code to use"

lang=grc
textfile=""
fontdir=""
savedir=""

while `test -n "$1"`; do
	if `test "$1" = "-l"`; then
		shift
		lang="$1"
	elif `test -z "$fontdir"`; then
		fontdir="$1"
	elif `test -z "$textfile"`; then
		textfile="$1"
	else
		savedir="$1"
	fi
	shift
done
test -z "$textfile" -o -z "$fontdir" -o -z "$savedir" && echo "$usage" && exit 1

mkdir -p "$savedir"

# set fontconfig to use fontdir
t=`mktemp`
tdir=`mktemp -d`
printf '<?xml version="1.0"?><!DOCTYPE fontconfig SYSTEM "fonts.dtd">
        <fontconfig><dir>%s</dir><cachedir>%s</cachedir></fontconfig>' \
        "$fontdir" "$tdir" > "$t"
export FONTCONFIG_FILE="$t"

fontlist=`fc-list : family | awk -F , '{print $1}' | sort | uniq`

echo "$fontlist" | while read font; do
	shortname=`echo "$font" | sed 's/[ \-]//g' | tr A-Z a-z`
	startname="$lang.$shortname.exp0"

	echo "$font - generating image and box"
	lazytrain "$textfile" "$font" \
	  "$savedir/$startname.png" "$savedir/$startname.box"
	echo "$shortname" 0 0 0 1 0 >> "$savedir/font_properties"
done
rm -rf "$t" "$tdir"
