/*
 * Copyright 2012 Nick White <nick.white@durham.ac.uk>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * This reads an image file and uzn file, and outputs an image file
 * overlaid with segment outlines from the uzn.
 *
 * Build it with something like this:
 * cc `pkg-config --cflags --libs pangocairo` uzndraw.c -o uzndraw
 */

#define usage "uzndraw - overlays an image with uzn outlines\n" \
              "usage: uzndraw in.png in.uzn out.png\n"

#include <stdio.h>
#include <stdlib.h>
#include <cairo.h>
#include <pango/pangocairo.h>

#define MAXUZN 1024

typedef struct {
	unsigned int x;
	unsigned int y;
	unsigned int w;
	unsigned int h;
} Uzn;

int main(int argc, char *argv[]) {
	char buf[BUFSIZ];
	unsigned int uznlen, i;
	Uzn uzn[MAXUZN];
	Uzn *u;
	FILE *uznfile;
	cairo_surface_t *surface;
	cairo_t *cr;
	cairo_status_t err;
	PangoLayout *layout;

	if(argc != 4) {
		fputs(usage, stdout);
		return 1;
	}

	if((uznfile = fopen(argv[2], "r")) == NULL) {
		fprintf(stderr, "Can't open uzn file: %s\n", argv[2]);
		return 1;
	}

	/* parse uzn file into uzn struct */
	for(uznlen=0, u=uzn; !feof(uznfile) && uznlen < MAXUZN; uznlen++, u++) {
		if(fgets(buf, sizeof(buf) - 1, uznfile) == NULL) {
			break;
		}
		sscanf(buf, "%u %u %u %u",
		       &(u->x), &(u->y), &(u->w), &(u->h));
	}
	fclose(uznfile);

	surface = cairo_image_surface_create_from_png(argv[1]);
	if((err = cairo_surface_status(surface)) != CAIRO_STATUS_SUCCESS) {
		fprintf(stderr, "Error opening image file: %s - %s\n",
		        argv[1], cairo_status_to_string(err));
		return 1;
	}
	cr = cairo_create(surface);
	layout = pango_cairo_create_layout(cr);

	cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
	for(i=0, u=uzn; i<uznlen; i++, u++) {
		cairo_rectangle(cr, u->x, u->y, u->w, u->h);
		cairo_stroke(cr);
	}

	g_object_unref(layout);                                                                    
	cairo_destroy(cr);                                                                         
	if((err = cairo_surface_write_to_png(surface, argv[3])) != CAIRO_STATUS_SUCCESS) {         
		fprintf(stderr, "Error writing image file: %s\n", cairo_status_to_string(err));    
	}                                                                                          
	cairo_surface_destroy(surface);

	return 0;
}
