PREFIX=/usr/local
PKG_CONFIG = pkg-config
CFLAGS = -Wall -g
LDFLAGS =

CAIROCFLAGS = `$(PKG_CONFIG) --cflags pangocairo`
CAIROLDFLAGS = `$(PKG_CONFIG) --libs pangocairo`
UTFSRC = libutf/rune.c libutf/utf.c

BINS = boxer uzndraw hocrdraw lazytrain isupper charmetrics

all: $(BINS)

boxer: boxer.c
	$(CC) $(CFLAGS) $(CAIROCFLAGS) $@.c -o $@ $(LDFLAGS) $(CAIROLDFLAGS)

uzndraw: uzndraw.c
	$(CC) $(CFLAGS) $(CAIROCFLAGS) $@.c -o $@ $(LDFLAGS) $(CAIROLDFLAGS)

# technically this only needs cairo-png, not pangocairo
hocrdraw: hocrdraw.c
	$(CC) -g $(CFLAGS) $(CAIROCFLAGS) $@.c -o $@ $(LDFLAGS) $(CAIROLDFLAGS)

lazytrain: lazytrain.c
	$(CC) $(CFLAGS) $(CAIROCFLAGS) $(UTFSRC) util/runetype.c $@.c -o $@ $(LDFLAGS) $(CAIROLDFLAGS)

charmetrics: charmetrics.c
	$(CC) $(CFLAGS) $(CAIROCFLAGS) $(UTFSRC) $@.c -o $@ $(LDFLAGS) $(CAIROLDFLAGS)

isupper: isupper.c
	$(CC) $(CFLAGS) $(UTFSRC) util/runetype.c $< -o $@ $(LDFLAGS)

install: all
	mkdir -p $(PREFIX)/bin
	cp $(BINS) $(PREFIX)/bin/

clean:
	rm -f $(BINS)
