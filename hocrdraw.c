/*
 * Copyright 2012 Nick White <nick.white@durham.ac.uk>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * Build it with something like this:
 * cc `pkg-config --cflags --libs cairo-png` hocrdraw.c -o hocrdraw
 */

#define usage "hocrdraw - overlays an image with lines from a hocr file\n" \
              "usage: hocrdraw in.png hocr.htm out.png\n"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cairo.h>

int main(int argc, char *argv[]) {
	char *buf;
	char *s;
	unsigned int buflen;
	long x1, y1, x2, y2;
	FILE *hocr;
	cairo_surface_t *surface;
	cairo_t *cr;
	cairo_status_t err;

	if(argc != 4) {
		fputs(usage, stdout);
		return 1;
	}

	if((hocr = fopen(argv[2], "r")) == NULL) {
		fprintf(stderr, "Can't open hocr file: %s\n", argv[2]);
		return 1;
	}

	surface = cairo_image_surface_create_from_png(argv[1]);
	if((err = cairo_surface_status(surface)) != CAIRO_STATUS_SUCCESS) {
		fprintf(stderr, "Error opening image file: %s - %s\n",
		        argv[1], cairo_status_to_string(err));
		fclose(hocr);
		return 1;
	}
	cr = cairo_create(surface);

	cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);

	/* read all of hocr into buf */
	buflen = BUFSIZ;
	buf = (char *)realloc(NULL, sizeof(*buf) * buflen);
	s = buf;
	while(fread(s, BUFSIZ, 1, hocr) == 1) {
		buflen += BUFSIZ;
		buf = (char *)realloc(buf, sizeof(*buf) * buflen);
		s = buf+(buflen-BUFSIZ);
		memset(s, 0, BUFSIZ);
	}
	fclose(hocr);

	s = buf;
	while(s < buf+buflen) {
		if((s = strstr(s, "<span class='ocr_line'")) == NULL)
			break;
		s+=strlen("<span class='ocr_line'");
		/* we're assuming all ocr_lines have title with bbox, but 
		 * that's safe for our purposes (tesseract always will) */
		if((s = strstr(s, "title=\"bbox ")) == NULL)
			break;
		s+=strlen("title=\"bbox ");
		sscanf(s, "%ld %ld %ld %ld", &x1, &y1, &x2, &y2);

		cairo_rectangle(cr, x1, y1, x2 - x1, y2 - y1);
		cairo_stroke(cr);
	}

	free(buf);
	cairo_destroy(cr);                                                                         
	if((err = cairo_surface_write_to_png(surface, argv[3])) != CAIRO_STATUS_SUCCESS) {         
		fprintf(stderr, "Error writing image file: %s\n", cairo_status_to_string(err));    
	}                                                                                          
	cairo_surface_destroy(surface);

	return 0;
}
