#!/bin/sh
# Copyright 2012-2014 Nick White <nick.white@durham.ac.uk>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

usage="Usage: $0 allchars

Prints every combination of two characters given, separated by newlines.

Needs isupper in its path."

test $# -ne 1 && echo "$usage" && exit 1

charlist="$1"

cat "$charlist" | while read char1; do
	cat "$charlist" | while read char2; do
		isupper "$char2" && continue

		printf "%s%s\n" "$char1" "$char2"
	done
done
