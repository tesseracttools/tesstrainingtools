#!/bin/sh
# Copyright 2012 Nick White <nick.white@durham.ac.uk>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

usage="Usage: $0 [-l langcode] trainingdir1 [trainingdir2...]

Creates a Tesseract 3 traineddata file.

Looks for .png, .box, font_properties, freq-words, all-words,
config, number-list punc-list and unicharambigs files in each
training directory.

Note that if files of the same name are found in different training
directories they won't be copied, so ensure there are no name collisions."

lang=grc

test $# -lt 1 && echo "$usage" && exit 1

t=`mktemp -d`
orig=`pwd`

echo "Combining training source files"
while `test -n "$1"`; do
	if `test "$1" = "-l"`; then
		shift
		lang="$1"
	else
		# copy all files with these extensions
		for i in png box config unicharambigs; do
			if ls "$1"/*$i >/dev/null 2>&1; then
				cp "$1"/*$i "$t/"
			fi
		done
		# ensure font_properties entries aren't duplicated
		if test -f "$1"/font_properties; then
			while read f; do
				fn=`echo "$f" | awk '{print $1}'`
				if ! `grep "$fn " "$t"/font_properties >/dev/null 2>&1`; then
					echo "$f" >> "$t"/font_properties
				fi
			done < "$1"/font_properties
		fi
		for i in freq-words all-words punc-list number-list; do
			test -f "$1/$i" && cat "$1/$i" >> "$t/$i"
		done
	fi
	shift
done

cd "$t"

echo "Making .tr trainings"
for i in *png; do
	b=`basename "$i" .png`
	tesseract "$i" "$b" nobatch box.train.stderr
done

echo "Getting list of all possible characters"
unicharset_extractor *box >&2

echo "Clustering"
shapeclustering -F font_properties -U unicharset $lang*tr
mftraining -F font_properties -U unicharset -O $lang.unicharset $lang*tr
cntraining $lang*tr

echo "Creating word lists"
wordlist2dawg freq-words $lang.freq-dawg $lang.unicharset
wordlist2dawg all-words $lang.word-dawg $lang.unicharset
wordlist2dawg punc-list $lang.punc-dawg $lang.unicharset
wordlist2dawg number-list $lang.number-dawg $lang.unicharset

echo "Combining training files"
for i in shapetable normproto inttemp pffmtable; do
	mv "$i" "$lang.$i"
done
combine_tessdata $lang.

cd "$orig"
cp "$t/$lang.traineddata" .
echo "Done. $lang.traineddata should now be usable"
